//
// Created by Rafael Pimentel on 07/06/2019.
//

#ifndef PROJETOFINALCONWAYSGAMEOFLIFE_LEMONS_H
#define PROJETOFINALCONWAYSGAMEOFLIFE_LEMONS_H

//Define o Tabuleiro
#define  col 20
#define row 22


int earth[col][row];
int earth2[col][row];

//Mostra o Menu
void menu();

//Inicializa um tabuleiro aleatório com um número de células vivas definidas pelo jogador
void RandPop(int x, int y, int nPop);


//Começa o jogo, com numero de colunas e linhas e quantas iterações
void Play(int Gens);

//Calcula os vizinhos
int neighbours(int x, int y);

//Número de Células Vivas
int living();

//Mete o tabuleiro a zero
void clorox();

//Imprime o Tabuleiro inicial
void Gen0(int x, int y);

//Verifica se houve mudança entre gerações
int Evolution();

#endif //PROJETOFINALCONWAYSGAMEOFLIFE_LEMONS_H
