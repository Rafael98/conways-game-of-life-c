//
// Created by Rafael Pimentel on 07/06/2019.
//

#include "lemons.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

void menu() {
    printf("Welcome to Conway's Game of Life!\n");
    char opc;
    int nPop = 0; //Variavel para numero da População
    int nGens = 0; //Variavel para o numero de interações (Generations)
    int nx = 0, ny = 0; //Variáveis para o utilizador inserir as cordenadas onde quer células
    system("cls");
    printf("-----------------------------------------------------------------------------------------------"
           "\n\t\t\t\t\t| Menu |"
           "\n-----------------------------------------------------------------------------------------------"
           "\n1 - Jogar"
           "\n2 - Regras do Jogo"
           "\n3 - Sair");
    scanf(" %c", &opc);
    switch (opc) {
        case '1':
            system("cls");
            printf("-----------------------------------------------------------------------------------------------"
                   "\n\t\t\t\t\t| Modos de Jogo |"
                   "\n-----------------------------------------------------------------------------------------------"
                   "\n1 - Aleatorio"
                   "\n2 - Inserir Celulas"
                   //   "\n3 - Ficheiro"
                   //   "\n4 - Minhas regras"
                   "\n3 - Exit");
            scanf(" %c", &opc);
            switch (opc) {
                case '1':
                    system("cls");
                    clorox();  //Inicializa o Tabuleiro a zeros
                    printf("-----------------------------------------------------------------------------------------------"
                           "\nQuantas celulas vivas quer? (se escolher 0 sera atribuido um numero aleatorio)"
                           "\n-----------------------------------------------------------------------------------------------");
                    scanf("%d", &nPop);
                    printf("-----------------------------------------------------------------------------------------------"
                           "\nQuantas Iteracoes quer? (se escolher 0 sera iterado 10 vezes)"
                           "\n-----------------------------------------------------------------------------------------------");
                    scanf("%d", &nGens);
                    RandPop(col, row, nPop); //Inicializa o tabuleiro com celulas em coordenadas aleatórias
                    Play(nGens); //Joga o numero de interações pedidas pelo utilizador
                    printf("-----------------------------------------------------------------------------------------------"
                           "\nPressione qualquer tecla para voltar ao menu"
                           "\n-----------------------------------------------------------------------------------------------");
                    scanf(" %c", &opc);
                    menu(); //Volta ao inicio
                    break;
                case '2':
                    system("cls");
                    clorox(); //Inicializa o Tabuleiro a zeros
                    printf("-----------------------------------------------------------------------------------------------"
                           "\nInsere as posições x e y, lembre-se que o tabuleiro e de %d x %d sendo a primera casa 0"
                           "\n(Formato: XX XX ex: 6 12 Quando acabar envie 1111 1111)"
                           "\n-----------------------------------------------------------------------------------------------",
                           col, row);
                    scanf("%d %d", &nx, &ny);
                    do {
                        earth[nx][ny] = 1;
                        scanf("%d %d", &nx, &ny);
                    } while (nx != 1111 && ny != 1111);
                    printf("-----------------------------------------------------------------------------------------------"
                           "\nQuantas Iteracoes quer? (se escolher 0 sera iterado 10 vezes)"
                           "\n-----------------------------------------------------------------------------------------------");
                    scanf("%d", &nGens);
                    Gen0(col, row);  //Imprime a Geração 0, o tabuleiro com as células inseridas pelo utilizador
                    Play(nGens); //Joga o numero de interações pedidas pelo utilizador

                    printf("-----------------------------------------------------------------------------------------------"
                           "\nPressione qualquer tecla para voltar ao menu"
                           "\n-----------------------------------------------------------------------------------------------");
                    scanf(" %c", &opc);
                    menu();
                    break;
                case '3':
                    break;
//                case '4':
//                    break;
                default:
                    break;
            }
            break;
        case '2':
            system("cls");
            printf("-----------------------------------------------------------------------------------------------"
                   "\n\t\t\t\t\t| Regras |"
                   "\n-----------------------------------------------------------------------------------------------"
                   "\ndefine-se a vizinhanca de uma celula como as 8 posicoes que a envolvem;"
                   "\numa celula morre por solidao se tiver menos que 2 celulas vizinhas;"
                   "\numa celula morre por sobrelotacao se tiver mais de 3 celulas vizinhas;"
                   "\numa celula nasce numa posicao vazia se tiver exatamente 3 celulas vizinhas;"
                   "\n-----------------------------------------------------------------------------------------------"
                   "\nv-Voltar");
            scanf(" %c", &opc);
            menu();
            break;
        case '3':
            break;
        default:
            system("cls");
            menu();
            break;
    }
}

void RandPop(int x, int y, int nPop) {
    if (!nPop) nPop = rand() % (col * row + 1);
    //Gera um número aleatório dentro dos limites e adiciona ao tabuleiro
    for (int i = 0; i < nPop; i++) {
        int rndC = rand() % col;
        int rndR = rand() % row;
        if (earth[rndC][rndR] == 0) {
            earth[rndC][rndR] = 1;
        } else {
            nPop++;
        }
    }
    Gen0(x, y);
}

void Gen0(int x, int y) {
    //Imprime na Consola o tabuleiro
    printf("\nGeracao: %d\n", 0);
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; ++j) {
            if (earth[i][j]) {
                printf("%c ", 178);
            } else {
                printf("%c ", 176);
            }
        }
        printf("\n");
    }
    printf("Populacao: %d\n", living());
}

void Play(int Gens) {
    if (!Gens) Gens = 10;
    for (int i = 0; i < Gens; ++i) {
        printf("\nGeracao %d\n", i + 1);
        for (int j = 0; j < col; ++j) {
            for (int k = 0; k < row; ++k) {
                int knock = neighbours(j, k);
                if (knock < 2) {
                    earth2[j][k] = 0;
                } else if (knock > 3) {
                    earth2[j][k] = 0;
                } else if (knock == 3) {
                    earth2[j][k] = 1;
                }

                if (earth2[j][k] == 1) {
                    printf("%c ", 178);
                } else {
                    printf("%c ", 176);
                }

            }
            printf("\n");
        }
        for (int j = 0; j < col; ++j) {
            for (int k = 0; k < row; ++k) {
                earth[j][k] = earth2[j][k];
            }
        }
        int lvg = living();
        printf("Populacao : %d\n", lvg);
        if (lvg == 0) break;
    }
}

int neighbours(int x, int y) {
    int n = 0;
    if (x == 0 && y == 0) {
        //Canto Superior Esquerdo
        if (earth[x + 1][y] == 1) n++;
        if (earth[x][y + 1] == 1) n++;
        if (earth[x + 1][y + 1] == 1) n++;
        return n;
    } else if (x == col - 1 && y == 0) {
        //Canto Superior Direito
        if (earth[x - 1][y] == 1) n++;
        if (earth[x][y + 1] == 1) n++;
        if (earth[x - 1][y + 1] == 1) n++;
        return n;
    } else if (x == 0 && y == row - 1) {
        //Canto Inferiror Esquerdo
        if (earth[x + 1][y] == 1) n++;
        if (earth[x][y - 1] == 1) n++;
        if (earth[x + 1][y - 1] == 1) n++;
        return n;
    } else if (x == col - 1 && y == row - 1) {
        //Canto Inferiror Direito
        if (earth[x - 1][y]) n++;
        if (earth[x][y - 1]) n++;
        if (earth[x - 1][y - 1]) n++;
        return n;
    } else if (x == 0) {
        //Parede Esquerda
        if (earth[x][y - 1]) n++;
        if (earth[x][y + 1]) n++;
        if (earth[x + 1][y - 1]) n++;
        if (earth[x + 1][y + 1]) n++;
        if (earth[x + 1][y]) n++;
        return n;
    } else if (x == col - 1) {
        //Parede Direita
        if (earth[x][y - 1]) n++;
        if (earth[x][y + 1]) n++;
        if (earth[x - 1][y - 1]) n++;
        if (earth[x - 1][y + 1]) n++;
        if (earth[x - 1][y]) n++;
        return n;
    } else if (y == 0) {
        //Parede Superior
        if (earth[x - 1][y]) n++;
        if (earth[x + 1][y]) n++;
        if (earth[x - 1][y + 1]) n++;
        if (earth[x + 1][y + 1]) n++;
        if (earth[x][y + 1]) n++;
        return n;
    } else if (y == row - 1) {
        //Parede Inferior
        if (earth[x - 1][y]) n++;
        if (earth[x + 1][y]) n++;
        if (earth[x - 1][y - 1]) n++;
        if (earth[x + 1][y - 1]) n++;
        if (earth[x][y - 1]) n++;
        return n;
    } else {
        //Interior
        if (earth[x - 1][y - 1]) n++;
        if (earth[x][y - 1]) n++;
        if (earth[x + 1][y - 1]) n++;
        if (earth[x + 1][y]) n++;
        if (earth[x + 1][y + 1]) n++;
        if (earth[x][y + 1]) n++;
        if (earth[x - 1][y + 1]) n++;
        if (earth[x - 1][y]) n++;
        return n;
    }
}

int living() {
    int l = 0;
    for (int i = 0; i < col; ++i) {
        for (int j = 0; j < row; ++j) {
            l += earth[i][j];
        }
    }
    return l;
}

void clorox() {
    for (int i = 0; i < col; ++i) {
        for (int j = 0; j < row; ++j) {
            earth[i][j] = 0;
        }
    }
}